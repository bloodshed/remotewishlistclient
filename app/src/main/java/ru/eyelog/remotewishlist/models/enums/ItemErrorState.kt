package ru.eyelog.remotewishlist.models.enums

enum class ItemErrorState {
    GET_ALL_ITEMS,
    PUT_THE_ITEM,
    UPDATE_THE_ITEM,
    UPDATE_THE_ITEM_CHECK,
    DELETE_ITEM
}
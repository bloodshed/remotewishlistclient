package ru.eyelog.remotewishlist.models.to

class ListModel(
    val id: Int = 0,
    val title: String,
    val description: String,
    val numberOfElements: Int
)
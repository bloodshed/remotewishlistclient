package ru.eyelog.remotewishlist

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.DrawableImageViewTarget
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_list.*
import kotlinx.android.synthetic.main.activity_list.ivLoadingBar
import kotlinx.android.synthetic.main.activity_list.swipeRefreshLayout
import kotlinx.android.synthetic.main.activity_list.tvEmptyState
import ru.eyelog.remotewishlist.adapters.CheckListElementAdapter
import ru.eyelog.remotewishlist.dialogs.CreateListItemCheckDialog
import ru.eyelog.remotewishlist.models.dto.ItemDto
import ru.eyelog.remotewishlist.models.enums.ItemErrorState
import ru.eyelog.remotewishlist.models.to.ItemModel
import ru.eyelog.remotewishlist.net.ItemsApi
import ru.eyelog.remotewishlist.net.getRetrofitInstance
import ru.eyelog.remotewishlist.utils.SwipeGesture

class CheckListActivity : AppCompatActivity() {

    private var disposable: Disposable? = null
    private val api = getRetrofitInstance().create(ItemsApi::class.java)

    private val onItemClickAction = { model: ItemModel ->
        onListItemClicked(model)
    }
    private val onItemCheckBoxClickAction = { model: ItemModel, position: Int ->
        onListItemCheckBoxClicked(model, position)
    }
    private var isCreateItem = true
    private lateinit var alertDialog: AlertDialog.Builder

    private val onDialogAction = { item: ItemModel ->
        Log.i("Logcat", "List item $item")
        if(isCreateItem) {
            putElementIntoServer(item)
        } else {
            updateElementOnServer(item)
        }
    }

    private var currentList = mutableListOf<ItemModel>()

    private var currentParentId = 0

    private val adapter = CheckListElementAdapter(this, onItemClickAction, onItemCheckBoxClickAction)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        val gotTitle = intent.getStringExtra("title")
        tvTitleOfList.text = gotTitle
        currentParentId = intent.getIntExtra("parentId", 0)

        recyclerViewItem.layoutManager = LinearLayoutManager(this)
        recyclerViewItem.adapter = adapter

        alertDialog = AlertDialog.Builder(this)

        object : SwipeGesture(this, recyclerViewItem) {

            override fun instantiateUnderlayButton(
                viewHolder: RecyclerView.ViewHolder?,
                underlayButtons: MutableList<UnderlayButton>?
            ) {
                underlayButtons?.add(UnderlayButton(
                    "Delete",
                    AppCompatResources.getDrawable(
                        this@CheckListActivity,
                        R.drawable.ic_trash
                    ),
                    Color.parseColor("#000000"), Color.parseColor("#ffffff"),
                    object : UnderlayButton.UnderlayButtonClickListener {
                        override fun onClick(pos: Int) {
                            deleteItemDialog(pos)
                        }
                    }
                ))

                underlayButtons?.add(UnderlayButton(
                    "Edit",
                    AppCompatResources.getDrawable(
                        this@CheckListActivity,
                        R.drawable.ic_edit
                    ),
                    Color.parseColor("#0000FF"), Color.parseColor("#ffffff"),
                    object : UnderlayButton.UnderlayButtonClickListener {
                        override fun onClick(pos: Int) {
                            updateItemDialog(pos)
                        }
                    }
                ))
            }
        }

        getAllItems()

        floatingActionCheckListButton.setOnClickListener {
            isCreateItem = true
            showDialog()
        }
        floatingActionCheckListButtonBack.setOnClickListener {
            finish()
        }

        val imageView: ImageView = findViewById<View>(R.id.ivLoadingBar) as ImageView
        val imageViewTarget = DrawableImageViewTarget(imageView)
        Glide.with(this).load(R.drawable.long_cat_mode).into(imageViewTarget)

        swipeRefreshLayout.setOnRefreshListener {
            getAllItems()
        }
    }

    private fun putElementIntoServer(item: ItemModel) {

        disposable = api.putItemList(mapElementModelToDto(item))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe( {
                getAllItems()
            },{
                Log.e("Logcat", "got error: $it")
                showErrorStateDialog(state = ItemErrorState.PUT_THE_ITEM, model = item)
            })
    }

    private fun getAllItems() {
        disposable = api.getItemsList(currentParentId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                ivLoadingBar.visibility = View.VISIBLE
            }
            .doFinally {
                ivLoadingBar.visibility = View.GONE
                swipeRefreshLayout.isRefreshing = false
            }
            .map { items ->
                items.items.map { mapElementDtoToModel(it) }
            }
            .subscribe( { data ->
                Log.i("Logcat", "got response: $data")
                if(data.isEmpty()){
                    tvEmptyState.visibility = View.VISIBLE
                    recyclerViewItem.visibility = View.GONE
                } else {
                    tvEmptyState.visibility = View.GONE
                    recyclerViewItem.visibility = View.VISIBLE
                    currentList = data.toMutableList()
                    adapter.setContent(data.toList())
                    adapter.notifyDataSetChanged()
                }
            },{
                Log.e("Logcat", "got error: $it")
                showErrorStateDialog(state = ItemErrorState.GET_ALL_ITEMS)
            })
    }

    private fun updateElementOnServer(item: ItemModel) {

        disposable = api.updateItemOfLists(mapElementModelToDto(item))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe( {
                getAllItems()
            },{
                Log.e("Logcat", "got error: $it")
                showErrorStateDialog(state = ItemErrorState.UPDATE_THE_ITEM, model = item)
            })
    }

    private fun mapElementDtoToModel(item: ItemDto): ItemModel {
        return ItemModel(
            id = item.id,
            parentId = currentParentId,
            title = item.title,
            description = item.description,
            check = item.check != 0
        )
    }

    private fun mapElementModelToEntity(item: ItemModel): ItemModel {
        return ItemModel(
            id = item.id,
            parentId = currentParentId,
            title = item.title,
            description = item.description,
            check = item.check
        )
    }

    private fun mapElementModelToDto(item: ItemModel): ItemDto {
        return ItemDto(
            id = item.id,
            parent_id = currentParentId,
            title = item.title,
            description = item.description,
            check = if (item.check) 1 else 0
        )
    }

    private fun mapElementModelToEntityForUpdate(item: ItemModel): ItemModel {
        return ItemModel(
            id = item.id,
            parentId = currentParentId,
            title = item.title,
            description = item.description,
            check = item.check
        )
    }

    private fun onListItemClicked(model: ItemModel) {
        Log.i("Logcat", "onListItemClicked $model")
    }

    private fun onListItemCheckBoxClicked(model: ItemModel, position: Int) {
        Log.i("Logcat", "onListItemClicked $model, position $position")
        disposable = api.updateCheckItem(model.id, model.check)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                currentList[position].isLoading = true
                adapter.setContent(currentList.toList())
                adapter.notifyItemChanged(position)
            }
            .subscribe( {
                currentList[position].isLoading = false
                currentList[position].check = model.check
                adapter.setContent(currentList.toList())
                adapter.notifyItemChanged(position)
            },{
                Log.e("Logcat", "got error: $it")
                showErrorStateDialog(state = ItemErrorState.UPDATE_THE_ITEM_CHECK, model = model, position = position)
            })
    }

    private fun showDialog() {
        CreateListItemCheckDialog(onDialogAction).show(supportFragmentManager, "dialog")
    }

    private fun updateItemDialog(position: Int) {
        Log.i("Logcat", "updateItem position $position")
        isCreateItem = false
        CreateListItemCheckDialog(onDialogAction, currentList[position]).show(supportFragmentManager, "dialog")
    }

    private fun deleteItemDialog(position: Int) {
        Log.i("Logcat", "deleteItem position $position")
        alertDialog
            .setTitle("Delete?")
            .setMessage("Are you sure you want to delete?")
            .setPositiveButton("Yes") { _, _ ->
                val itemToDelete = mapElementModelToEntity(currentList[position])
                deleteItem(itemToDelete.id)

            }
            .setNegativeButton("No") { dialog, _ ->
                dialog.cancel()
            }

        alertDialog.create()
        alertDialog.show()
    }

    private fun deleteItem(id: Int){
        disposable = api.deleteItemOfLists(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe( {
                getAllItems()
            },{
                Log.e("Logcat", "got error: $it")
                showErrorStateDialog(state = ItemErrorState.DELETE_ITEM, id = id)
            })
    }

    private fun showErrorStateDialog(state: ItemErrorState, id: Int? = null, model: ItemModel? = null, position: Int? = null) {
        alertDialog
            .setTitle("Something got wrong")
            .setMessage("Retry?")
            .setPositiveButton("Yes") { _, _ ->
                errorStateController(state = state, id = id, model = model, position = position)
            }
            .setNegativeButton("No") { dialog, _ ->
                dialog.cancel()
            }

        alertDialog.create()
        alertDialog.show()
    }

    private fun errorStateController(state: ItemErrorState, id: Int? = null, model: ItemModel? = null, position: Int? = null){
        when(state){
            ItemErrorState.GET_ALL_ITEMS -> {
                getAllItems()
            }
            ItemErrorState.PUT_THE_ITEM -> {
                model?.let{ putElementIntoServer(model) }
            }
            ItemErrorState.UPDATE_THE_ITEM -> {
                model?.let{ updateElementOnServer(model) }
            }
            ItemErrorState.UPDATE_THE_ITEM_CHECK -> {
                if (model != null && position != null){
                    onListItemCheckBoxClicked(model, position)
                }
            }
            ItemErrorState.DELETE_ITEM -> {
                id?.let{ deleteItem(id) }
            }
        }
    }
}
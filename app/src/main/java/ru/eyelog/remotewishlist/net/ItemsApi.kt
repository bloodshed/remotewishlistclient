package ru.eyelog.remotewishlist.net

import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.*
import ru.eyelog.remotewishlist.models.dto.ItemDto
import ru.eyelog.remotewishlist.models.dto.ItemsDto
import ru.eyelog.remotewishlist.models.dto.ListDto
import ru.eyelog.remotewishlist.models.dto.ListsDto

interface ItemsApi {

    @GET("get_item_list.php")
    fun getItemsList(@Query("id") id: Int): Single<ItemsDto?>

    @POST("put_item_of_item_list.php")
    fun putItemList(@Body itemDto: ItemDto): Completable

    @POST("update_item_of_item_list.php")
    fun updateItemOfLists(@Body item: ItemDto): Completable

    @GET("update_check_item.php")
    fun updateCheckItem(@Query("id") id: Int, @Query("check") check: Boolean): Completable

    @GET("delete_item_of_item_list.php")
    fun deleteItemOfLists(@Query("id") id: Int): Completable
}
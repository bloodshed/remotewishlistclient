package ru.eyelog.remotewishlist.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.DrawableImageViewTarget
import ru.eyelog.remotewishlist.R
import ru.eyelog.remotewishlist.models.to.ItemModel

class CheckListElementAdapter(
    private val context: Context,
    private val onItemClicked: (ItemModel) -> Unit,
    private val onCheckBoxClicked: (ItemModel, Int) -> Unit
) : RecyclerView.Adapter<CheckListElementAdapter.CheckListElementHolder>() {

    var elementList = listOf<ItemModel>()

    fun setContent(content: List<ItemModel>) {
        elementList = content
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CheckListElementHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_check_list_element, parent, false)
        return CheckListElementHolder(context, itemView)
    }


    override fun onBindViewHolder(holder: CheckListElementHolder, position: Int) {
        holder.title.text = elementList[position].title
        holder.description.text = elementList[position].description
        holder.check.isChecked = elementList[position].check

        if (elementList[position].isLoading){
            holder.check.visibility = View.GONE
            holder.ivLoader.visibility = View.VISIBLE
        } else {
            holder.check.visibility = View.VISIBLE
            holder.ivLoader.visibility = View.GONE
        }

        holder.check.setOnCheckedChangeListener { _, _ ->
            onCheckBoxClicked(elementList[position].copy(check = holder.check.isChecked), position)
        }

        holder.itemView.setOnClickListener {
            onItemClicked(elementList[position])
        }
    }

    override fun getItemCount(): Int {
        return elementList.size
    }

    class CheckListElementHolder(
        context: Context,
        item: View
    ) : RecyclerView.ViewHolder(item) {

        val title: TextView = item.findViewById(R.id.tvCheckListTitle)
        val description: TextView = item.findViewById(R.id.tvCheckListDescription)
        val check: CheckBox = item.findViewById(R.id.checkBox)
        val ivLoader: ImageView = item.findViewById(R.id.ivLoadingCheck)

        init {
            val imageViewTarget = DrawableImageViewTarget(ivLoader)
            Glide.with(context).load(R.drawable.loading).into(imageViewTarget)
        }
    }
}


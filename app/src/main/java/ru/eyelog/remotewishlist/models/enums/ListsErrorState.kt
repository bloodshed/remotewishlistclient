package ru.eyelog.remotewishlist.models.enums

enum class ListsErrorState {
    GET_ALL_LISTS,
    PUT_THE_LIST,
    UPDATE_THE_LIST,
    DELETE_LIST
}
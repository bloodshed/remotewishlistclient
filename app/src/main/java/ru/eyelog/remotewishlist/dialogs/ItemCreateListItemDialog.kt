package ru.eyelog.remotewishlist.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.dialog_crate_list_element.*
import ru.eyelog.remotewishlist.R
import ru.eyelog.remotewishlist.models.to.ListModel


class ItemCreateListItemDialog(
    private val onItemCrateAction: (ListModel) -> Unit
) : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.dialog_crate_list_element, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btCreate.setOnClickListener {
            if (etTitle.text.isBlank()) {
                etTitle.error = "Enter test"
                return@setOnClickListener
            }

            if (etDescription.text.isBlank()) {
                etDescription.error = "Enter test"
                return@setOnClickListener
            }

            onItemCrateAction.invoke(
                ListModel(
                    title = etTitle.text.toString(),
                    description = etDescription.text.toString(),
                    numberOfElements = 0
                )
            )

            dismiss()
        }

        btCancel.setOnClickListener {
            this.dismiss()
        }
    }
}
package ru.eyelog.remotewishlist.adapters

import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import ru.eyelog.remotewishlist.R
import ru.eyelog.remotewishlist.models.to.ListModel

class ListElementAdapter (
    private val onItemClicked: (ListModel) -> Unit

) : RecyclerView.Adapter<ListElementAdapter.ListElementHolder>() {

    var elementList = listOf<ListModel>()

    fun setContent(content: List<ListModel>) {
        elementList = content
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListElementHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list_element, parent, false)
        return ListElementHolder(itemView)
    }

    override fun onBindViewHolder(holder: ListElementHolder, position: Int) {
        holder.title.text = elementList[position].title
        holder.description.text = elementList[position].description
        holder.number.text = elementList[position].numberOfElements.toString()

        holder.itemView.setOnClickListener {
            onItemClicked(elementList[position])
        }
    }

    override fun getItemCount(): Int {
        return elementList.size
    }

    class ListElementHolder(
        item: View
    ) : RecyclerView.ViewHolder(item), View.OnCreateContextMenuListener {

        val title = item.findViewById<TextView>(R.id.tvTitle)
        val description = item.findViewById<TextView>(R.id.tvDescription)
        val number = item.findViewById<TextView>(R.id.tvNumber)
        val card = item.findViewById<MaterialCardView>(R.id.cardView)

        init {
            card.setOnCreateContextMenuListener(this)
        }

        override fun onCreateContextMenu(
            menu: ContextMenu?,
            view: View?,
            info: ContextMenu.ContextMenuInfo?
        ) {
            menu?.add(this.bindingAdapterPosition, 121, 0, "Update item")
            menu?.add(this.bindingAdapterPosition, 122, 1, "Delete item")
        }
    }
}
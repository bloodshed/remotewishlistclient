package ru.eyelog.remotewishlist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.DrawableImageViewTarget
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.ivLoadingBar
import kotlinx.android.synthetic.main.activity_main.swipeRefreshLayout
import kotlinx.android.synthetic.main.activity_main.tvEmptyState
import ru.eyelog.remotewishlist.adapters.ListElementAdapter
import ru.eyelog.remotewishlist.dialogs.CreateListItemDialog
import ru.eyelog.remotewishlist.models.dto.ListDto
import ru.eyelog.remotewishlist.models.dto.ListsDto
import ru.eyelog.remotewishlist.models.enums.ListsErrorState
import ru.eyelog.remotewishlist.models.to.ListModel
import ru.eyelog.remotewishlist.net.ListsApi
import ru.eyelog.remotewishlist.net.getRetrofitInstance

class MainActivity : AppCompatActivity() {

    private val api = getRetrofitInstance().create(ListsApi::class.java)

    private var disposable: Disposable? = null

    private val onItemClickAction = { model: ListModel ->
        onListItemClicked(model)
    }
    private var currentList = emptyList<ListModel>()
    private lateinit var alertDialog: AlertDialog.Builder

    private val adapter = ListElementAdapter(onItemClickAction)
    private var isCreateItem = true
    private val onDialogAction = { item: ListModel ->
        Log.i("Logcat", "List item $item")
        if(isCreateItem) {
            putElementIntoServer(item)
        } else {
            updateElementIntoServer(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        floatingActionButton.setOnClickListener {
            isCreateItem = true
            showDialog()
        }

        registerForContextMenu(recyclerView)

        alertDialog = AlertDialog.Builder(this)

        val imageView: ImageView = findViewById<View>(R.id.ivLoadingBar) as ImageView
        val imageViewTarget = DrawableImageViewTarget(imageView)
        Glide.with(this).load(R.drawable.long_cat_mode).into(imageViewTarget)

        swipeRefreshLayout.setOnRefreshListener {
            getAllItems()
        }
    }

    override fun onStart() {
        super.onStart()
        getAllItems()
    }

    override fun onStop() {
        disposable?.dispose()
        super.onStop()
    }

    private fun putElementIntoServer(item: ListModel) {

        disposable = api.putItemList(mapElementModelToDto(item))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe( {
                getAllItems()
            },{
                Log.e("Logcat", "got error: $it")
                showErrorStateDialog(state = ListsErrorState.PUT_THE_LIST, model = item)
            })
    }
    private fun updateElementIntoServer(item: ListModel) {

        disposable = api.updateItemOfLists(mapElementModelToDto(item))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe( {
                getAllItems()
            },{
                Log.e("Logcat", "got error: $it")
                showErrorStateDialog(state = ListsErrorState.UPDATE_THE_LIST, model = item)
            })
    }

    private fun getAllItems() {
        disposable = api.getItemsList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                ivLoadingBar.visibility = View.VISIBLE
            }
            .doFinally {
                ivLoadingBar.visibility = View.GONE
                swipeRefreshLayout.isRefreshing = false
            }
            .map {
                mapElementDtoToModel(it)
            }
            .subscribe( { data ->
                Log.i("Logcat", "got response: $data")

                if(data.isEmpty()){
                    tvEmptyState.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
                }else {
                    tvEmptyState.visibility = View.GONE
                    recyclerView.visibility = View.VISIBLE
                    currentList = data
                    adapter.setContent(data)
                    adapter.notifyDataSetChanged()
                }
            },{
                Log.e("Logcat", "got error: $it")
                showErrorStateDialog(state = ListsErrorState.GET_ALL_LISTS)
            })
    }

    private fun deleteItemOfLists(id: Int){
        disposable = api.deleteItemOfLists(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe( {
                getAllItems()
            },{
                Log.e("Logcat", "got error: $it")
                showErrorStateDialog(state = ListsErrorState.DELETE_LIST, id = id)
            })
    }

    private fun mapElementModelToDto(item: ListModel): ListDto {
        return ListDto(
            id = item.id,
            title = item.title,
            description = item.description,
            numberOfElements = item.numberOfElements
        )
    }

    private fun mapElementDtoToModel(item: ListsDto): List<ListModel> {
        val outList = mutableListOf<ListModel>()
        item.items.forEach {
            outList.add(
                ListModel(
                    id = it.id,
                    title = it.title,
                    description = it.description,
                    numberOfElements = it.numberOfElements
                )
            )
        }
        return outList.toList()
    }

    private fun onListItemClicked(model: ListModel) {
        val intent = Intent(this, CheckListActivity::class.java)
        intent.putExtra("title", model.title)
        intent.putExtra("parentId", model.id)
        startActivity(intent)
    }

    private fun showDialog() {
        CreateListItemDialog(onDialogAction).show(supportFragmentManager, "dialog")
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            121 -> {
                updateItem(item.groupId)
                true
            }
            122 -> {
                showDeleteItemDialog(item.groupId)
                true
            }
            else -> super.onContextItemSelected(item)
        }
    }

    private fun updateItem(position: Int) {
        Log.i("Logcat", "updateItem position $position")
        isCreateItem = false
        CreateListItemDialog(onDialogAction, currentList[position]).show(supportFragmentManager, "dialog")
    }

    private fun showDeleteItemDialog(position: Int) {
        Log.i("Logcat", "deleteItem position ${currentList[position]}")
        alertDialog
            .setTitle("Delete?")
            .setMessage("Are you sure you want to delete?")
            .setPositiveButton("Yes") { _, _ ->
                deleteItemOfLists(currentList[position].id)
            }
            .setNegativeButton("No") { dialog, _ ->
                dialog.cancel()
            }

        alertDialog.create()
        alertDialog.show()
    }

    private fun showErrorStateDialog(state: ListsErrorState, id: Int? = null, model: ListModel? = null) {
        alertDialog
            .setTitle("Something got wrong")
            .setMessage("Retry?")
            .setPositiveButton("Yes") { _, _ ->
                errorStateController(state = state, id = id, model = model)
            }
            .setNegativeButton("No") { dialog, _ ->
                dialog.cancel()
            }

        alertDialog.create()
        alertDialog.show()
    }

    private fun errorStateController(state: ListsErrorState, id: Int? = null, model: ListModel? = null){
        when(state){
            ListsErrorState.GET_ALL_LISTS -> {
                getAllItems()
            }
            ListsErrorState.PUT_THE_LIST -> {
                model?.let{ putElementIntoServer(model) }
            }
            ListsErrorState.UPDATE_THE_LIST -> {
                model?.let{ updateElementIntoServer(model) }
            }
            ListsErrorState.DELETE_LIST -> {
                id?.let{ deleteItemOfLists(id) }
            }
        }
    }
}
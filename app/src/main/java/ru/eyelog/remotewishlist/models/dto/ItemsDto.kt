package ru.eyelog.remotewishlist.models.dto

import com.google.gson.annotations.SerializedName

data class ItemsDto(
    @SerializedName("items")
    val items: List<ItemDto>
)
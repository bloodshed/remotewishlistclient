package ru.eyelog.remotewishlist.models.dto

import com.google.gson.annotations.SerializedName

data class ItemDto(
    @SerializedName("id")
    val id: Int,
    @SerializedName("parent_id")
    val parent_id: Int,
    @SerializedName("title")
    val title: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("check")
    val check: Int
)

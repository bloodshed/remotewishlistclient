package ru.eyelog.remotewishlist.models.dto

import com.google.gson.annotations.SerializedName

data class ListsDto(
    @SerializedName("items")
    val items: List<ListDto>
)

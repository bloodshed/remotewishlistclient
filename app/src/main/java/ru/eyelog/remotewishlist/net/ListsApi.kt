package ru.eyelog.remotewishlist.net

import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.*
import ru.eyelog.remotewishlist.models.dto.ListDto
import ru.eyelog.remotewishlist.models.dto.ListsDto

interface ListsApi {

    @GET("get_list_of_lists.php")
    fun getItemsList(): Single<ListsDto?>

    @POST("put_item_of_list.php")
    fun putItemList(@Body itemDto: ListDto): Completable

    @POST("update_item_of_list.php")
    fun updateItemOfLists(@Body item: ListDto): Completable

    @GET("delete_item_of_lists.php")
    fun deleteItemOfLists(@Query("id") id: Int): Completable
}
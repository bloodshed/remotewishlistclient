package ru.eyelog.remotewishlist.utils

import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


abstract class SwipeGesture(
    context: Context,
    recyclerView: RecyclerView
): ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {

    private val BUTTON_WIDTH = 200
    private var recyclerViewLoc: RecyclerView? = null
    private var buttons: MutableList<UnderlayButton>? = null
    private var gestureDetector: GestureDetector? = null
    private var swipedPos = -1
    private var swipeThreshold = 0.5f
    private var buttonsBuffer: MutableMap<Int, MutableList<UnderlayButton>>? = null
    private var recoverQueue: Queue<Int>? = null

    private val gestureListener: GestureDetector.SimpleOnGestureListener = object : GestureDetector.SimpleOnGestureListener() {
        override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
            for (button in buttons!!) {
                if (button.onClick(e.x, e.y)) break
            }
            return true
        }
    }

    private val onTouchListener = View.OnTouchListener { v, e ->
        if (swipedPos < 0) return@OnTouchListener false

        val point = Point(e.rawX.toInt(), e.rawY.toInt())
        val swipedViewHolder = recyclerView.findViewHolderForAdapterPosition(swipedPos)
        val swipedItem = swipedViewHolder!!.itemView
        val rect = Rect()
        swipedItem.getGlobalVisibleRect(rect)
        if (e.action == MotionEvent.ACTION_DOWN || e.action == MotionEvent.ACTION_UP || e.action == MotionEvent.ACTION_MOVE) {
            if (rect.top < point.y && rect.bottom > point.y) gestureDetector!!.onTouchEvent(e) else {
                recoverQueue!!.add(swipedPos)
                swipedPos = -1
                recoverSwipedItem()
            }
        }
        v.performClick()
        true
    }

    init{
        buttons = ArrayList()
        gestureDetector = GestureDetector(context, gestureListener)
        recyclerViewLoc = recyclerView
        recyclerView.setOnTouchListener(onTouchListener)
        buttonsBuffer = HashMap()
        recoverQueue = object : LinkedList<Int>() {
            override fun add(o: Int): Boolean {
                return if (contains(o)) false else super.add(o)
            }
        }
        attachSwipe()
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val pos = viewHolder.bindingAdapterPosition
        if (swipedPos != pos) recoverQueue!!.add(swipedPos)
        swipedPos = pos
        buttonsBuffer?.let { buttonsBuffer ->
            if (buttonsBuffer.containsKey(swipedPos)) buttons =
                buttonsBuffer[swipedPos] else buttons?.clear()
            buttonsBuffer.clear()
            swipeThreshold = 0.5f * (buttons?.size ?: 0) * BUTTON_WIDTH
            recoverSwipedItem()
        }
    }

    override fun getSwipeThreshold(viewHolder: RecyclerView.ViewHolder): Float {
        return swipeThreshold
    }

    override fun getSwipeEscapeVelocity(defaultValue: Float): Float {
        return 0.1f * defaultValue
    }

    override fun getSwipeVelocityThreshold(defaultValue: Float): Float {
        return 5.0f * defaultValue
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        val pos = viewHolder.bindingAdapterPosition
        var translationX = dX
        val itemView = viewHolder.itemView
        if (pos < 0) {
            swipedPos = pos
            return
        }
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            if (dX > 0) {
                var buffer: MutableList<UnderlayButton> = ArrayList()
                if (!buttonsBuffer!!.containsKey(pos)) {
                    instantiateUnderlayButton(viewHolder, buffer)
                    buttonsBuffer!![pos] = buffer
                } else {
                    buffer = buttonsBuffer!![pos]!!
                }
                translationX = dX * buffer.size * BUTTON_WIDTH / itemView.width
                drawButtons(c, itemView, buffer, pos, translationX)
            }
        }
        super.onChildDraw(
            c,
            recyclerView, viewHolder, translationX, dY, actionState, isCurrentlyActive
        )
    }

    @Synchronized
    open fun recoverSwipedItem() {
        recoverQueue?.let { recoverQueue ->
            while (!recoverQueue.isEmpty()) {
                val pos = recoverQueue.poll()
                if (pos != null) {
                    if (pos > -1) {
                        recyclerViewLoc?.adapter?.notifyItemChanged(pos)
                    }
                }
            }
        }
    }

    open fun drawButtons(
        c: Canvas,
        itemView: View,
        buffer: List<UnderlayButton>,
        pos: Int,
        dX: Float
    ) {
        var left = itemView.left.toFloat()
        val dButtonWidth = dX / buffer.size
        for (button in buffer) {
            val right = left + dButtonWidth
            button.onDraw(
                c,
                RectF(
                    left,
                    itemView.top.toFloat(),
                    right,
                    itemView.bottom.toFloat()
                ),
                pos
            )
            left = right
        }
    }

    private fun attachSwipe() {
        val itemTouchHelper = ItemTouchHelper(this)
        itemTouchHelper.attachToRecyclerView(recyclerViewLoc)
    }

    abstract fun instantiateUnderlayButton(
        viewHolder: RecyclerView.ViewHolder?,
        underlayButtons: MutableList<UnderlayButton>?
    )

    open class UnderlayButton(
        private val text: String,
        private val imageResId: Drawable?,
        private val buttonBackgroundColor: Int,
        private val textColor: Int,
        private val clickListener: UnderlayButtonClickListener
    ) {
        private var pos = 0
        private var clickRegion: RectF? = null
        fun onClick(x: Float, y: Float): Boolean {
            if (clickRegion != null && clickRegion!!.contains(x, y)) {
                clickListener.onClick(pos)
                return true
            }
            return false
        }

        fun onDraw(canvas: Canvas, rect: RectF, pos: Int) {
            val p = Paint()
            // Draw background
            p.color = buttonBackgroundColor
            canvas.drawRect(rect, p)
            p.color = textColor
            p.textSize = 40f
            val r = Rect()
            val cHeight = rect.height()
            val cWidth = rect.width()
            p.textAlign = Paint.Align.LEFT
            p.getTextBounds(text, 0, text.length, r)
            val x = cWidth / 2f - r.width() / 2f + r.left
            val y = cHeight / 2f + r.height() / 2f - r.bottom - 40
            canvas.drawText(text, rect.left + x, rect.top + y, p)
            if (imageResId != null) {
                imageResId.setBounds(
                    (rect.left + 50).toInt(),
                    (rect.top + cHeight / 2f).toInt(),
                    (rect.right - 50).toInt(),
                    (rect.bottom - cHeight / 10f).toInt()
                )
                imageResId.draw(canvas)
            }
            clickRegion = rect
            this.pos = pos
        }

        interface UnderlayButtonClickListener {
            fun onClick(pos: Int)
        }
    }
}
package ru.eyelog.remotewishlist.models.to

data class ItemModel(
    val id: Int = 0,
    val parentId: Int = 0,
    val title: String,
    val description: String,
    var check: Boolean,
    var isLoading: Boolean = false,
)
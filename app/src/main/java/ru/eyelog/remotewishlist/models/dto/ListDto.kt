package ru.eyelog.remotewishlist.models.dto

import com.google.gson.annotations.SerializedName

data class ListDto(
    @SerializedName("id")
    val id: Int,
    @SerializedName("title")
    val title: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("numberOfElements")
    val numberOfElements: Int
)

package ru.eyelog.remotewishlist.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.dialog_create_check_list.*
import ru.eyelog.remotewishlist.R
import ru.eyelog.remotewishlist.models.to.ItemModel


class CreateListItemCheckDialog(
    private val onItemCrateAction: (ItemModel) -> Unit,
    private val model: ItemModel? = null
): DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.dialog_create_check_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (model == null){

            btCheckListCreate.setOnClickListener {
                if (etCheckListTitle.text.isBlank()) {
                    etCheckListTitle.error = "Enter test"
                    return@setOnClickListener
                }

                if (etCheckListDescription.text.isBlank()) {
                    etCheckListDescription.error = "Enter test"
                    return@setOnClickListener
                }

                onItemCrateAction.invoke(
                    ItemModel(
                        title = etCheckListTitle.text.toString(),
                        description = etCheckListDescription.text.toString(),
                        check = false
                    )
                )

                dismiss()
            }

        } else {
            tvMainCheckListTitle.text = "Update item"
            btCheckListCreate.text = "Update"
            etCheckListTitle.setText(model.title)
            etCheckListDescription.setText(model.description)

            btCheckListCreate.setOnClickListener {
                if (etCheckListTitle.text.isBlank()) {
                    etCheckListTitle.error = "Enter test"
                    return@setOnClickListener
                }

                if (etCheckListDescription.text.isBlank()) {
                    etCheckListDescription.error = "Enter test"
                    return@setOnClickListener
                }

                onItemCrateAction.invoke(
                    ItemModel(
                        id = model.id,
                        title = etCheckListTitle.text.toString(),
                        description = etCheckListDescription.text.toString(),
                        check = model.check

                    )
                )

                dismiss()
            }
        }

        btCheckListCancel.setOnClickListener {
            this.dismiss()
        }
    }
}
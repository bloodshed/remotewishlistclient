package ru.eyelog.remotewishlist.net

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

private const val BASE_URL = "http://192.168.1.42/"
private const val DEFAULT_TIMEOUT = 20

fun getRetrofitInstance(): Retrofit {

    val loggingInterceptor = HttpLoggingInterceptor()
    loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

    val requestInterceptor = Interceptor { chain ->
        val original: Request = chain.request()

        val request: Request = original.newBuilder()
            .header("Content-Type", "application/json")
            .build()
        chain.proceed(request)
    }

    val okHttpClient = OkHttpClient.Builder()
    okHttpClient.connectTimeout(DEFAULT_TIMEOUT.toLong(), TimeUnit.SECONDS)
    okHttpClient.addInterceptor(loggingInterceptor)
    okHttpClient.addInterceptor(requestInterceptor)

    return Retrofit.Builder().baseUrl(BASE_URL)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttpClient.build())
        .build()
}